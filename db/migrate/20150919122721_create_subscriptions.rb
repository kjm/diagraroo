class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.string :token
      t.integer :resource_id
      t.string :resource_type

      t.timestamps null: false
    end
  end
end
