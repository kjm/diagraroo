class CreateCanvasses < ActiveRecord::Migration
  def change
    create_table :canvasses do |t|
      t.string :label
      t.integer :x
      t.integer :y
      t.integer :canvas_type
      t.json :dimensions
      t.integer :diagram_id

      t.timestamps null: false
    end

    add_foreign_key :canvasses, :diagrams, on_delete: :cascade
  end
end
