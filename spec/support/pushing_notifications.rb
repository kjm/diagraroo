shared_examples_for 'pushing notifications' do |subscribers_count|
  it 'should push APN notification to subscribers' do
    expect(APNService).to receive(:push_notification).exactly(subscribers_count).times
    subject
  end
end
