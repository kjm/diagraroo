require 'rails_helper'

describe API::V1::Canvasses do
  let(:diagram) { FactoryGirl.create :diagram }

  describe 'GET /api/v1/canvasses/:id' do
    let!(:canvas) { FactoryGirl.create :circle_canvas }
    before(:each) do
      get "/api/v1/canvasses/#{canvas.id}"
    end

    it { expect(JSON.parse(response.body).fetch('canvas').fetch('id')).to eq(canvas.id) }
    it { expect(response.status).to eq(200) }
  end

  describe 'POST /api/v1/canvasses' do
    before(:each) { 5.times { FactoryGirl.create :subscription, resource: diagram } }
    let(:params) do
      {
        canvas: {
          label: 'A canvas',
          canvas_type: Canvas::CIRCLE,
          dimensions: { 'r' => 2 },
          diagram_id: diagram.id
        }
      }
    end
    subject { post '/api/v1/canvasses', params }

    it { expect { subject }.to change { Canvas.count }.by(1) }
    it do
      subject
      expect(response.status).to eq(201)
    end
    it_should_behave_like 'pushing notifications', 5
  end

  describe 'PATCH /api/v1/canvasses/:id' do
    let!(:canvas) { FactoryGirl.create :circle_canvas, diagram: diagram }
    before(:each) { 5.times { FactoryGirl.create :subscription, resource: diagram } }
    let(:params) do
      {
        canvas: {
          label: 'Other name',
          canvas_type: Canvas::CIRCLE,
          dimensions: { 'r' => 2 },
          diagram_id: diagram.id
        }
      }
    end
    subject { patch "/api/v1/canvasses/#{canvas.id}", params }

    it { expect { subject }.not_to change { Canvas.count } }
    it do
      subject
      expect(response.status).to eq(200)
    end
    it_should_behave_like 'pushing notifications', 5
  end

  describe 'DELETE /api/v1/canvasses/:id' do
    let!(:canvas) { FactoryGirl.create :circle_canvas, diagram: diagram }
    before(:each) { 5.times { FactoryGirl.create :subscription, resource: diagram } }
    subject { delete "/api/v1/canvasses/#{canvas.id}" }

    it { expect { subject }.to change { Canvas.count }.by(-1) }
    it do
      subject
      expect(response.status).to eq(200)
    end
    it_should_behave_like 'pushing notifications', 5
  end
end
