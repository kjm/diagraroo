require 'rails_helper'

describe API::V1::Diagrams do
  describe 'GET /api/v1/diagrams' do
    before(:each) do
      10.times { FactoryGirl.create :diagram }
      get '/api/v1/diagrams'
    end

    it { expect(response.status).to eq(200) }
    it { expect(JSON.parse(response.body).size).to eq(10) }
  end

  describe 'GET /api/v1/diagrams/:id' do
    let!(:diagram) { FactoryGirl.create :diagram }
    before(:each) do
      get "/api/v1/diagrams/#{diagram.id}"
    end

    it { expect(response.status).to eq(200) }
    it { expect(JSON.parse(response.body).fetch('diagram').fetch('id')).to eq(diagram.id) }
  end

  describe 'POST /api/v1/diagrams' do
    let(:params) do
      {
        diagram: {
          name: 'A diagram'
        }
      }
    end
    subject { post '/api/v1/diagrams', params }

    it do
      subject
      expect(response.status).to eq(201)
    end
    it { expect { subject }.to change { Diagram.count }.by(1) }
  end

  describe 'PATCH /api/v1/diagrams/:id' do
    let!(:diagram) { FactoryGirl.create :diagram }
    before(:each) { 5.times { FactoryGirl.create :subscription, resource: diagram } }
    let(:params) do
      {
        diagram: {
          name: 'Other name'
        }
      }
    end
    subject { patch "/api/v1/diagrams/#{diagram.id}", params }

    it do
      subject
      expect(response.status).to eq(200)
    end
    it { expect { subject }.not_to change { Diagram.count } }
    it_should_behave_like 'pushing notifications', 5
  end

  describe 'DELETE /api/v1/diagrams/:id' do
    let!(:diagram) { FactoryGirl.create :diagram }
    before(:each) { 5.times { FactoryGirl.create :subscription, resource: diagram } }
    subject { delete "/api/v1/diagrams/#{diagram.id}" }

    it do
      subject
      expect(response.status).to eq(200)
    end
    it { expect { subject }.to change { Diagram.count }.by(-1) }
    it_should_behave_like 'pushing notifications', 5
  end

  describe 'PATCH /api/v1/diagrams/:id/subscribe' do
    let!(:diagram) { FactoryGirl.create :diagram }
    subject { patch "/api/v1/diagrams/#{diagram.id}/subscribe", device: { token: '123' } }

    it do
      subject
      expect(response.status).to eq(200)
    end
    it { expect { subject }.to change { Subscription.count }.by(1) }
  end

  describe 'PATCH /api/v1/diagrams/:id/unsubscribe' do
    let!(:diagram) { FactoryGirl.create :diagram }
    let!(:subscription) { FactoryGirl.create :subscription, token: '123', resource: diagram }
    subject { patch "/api/v1/diagrams/#{diagram.id}/unsubscribe", device: { token: '123' } }

    it do
      subject
      expect(response.status).to eq(200)
    end
    it { expect { subject }.to change { Subscription.count }.by(-1) }
  end
end
