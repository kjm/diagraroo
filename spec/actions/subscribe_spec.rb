require 'rails_helper'

describe Subscribe do
  let(:diagram) { FactoryGirl.create :diagram }

  describe '#call' do
    context 'when valid device' do
      let(:device) do
        {
          token: '123 123 123'
        }
      end
      subject { Subscribe.new(diagram).call(device) }

      it { expect(subject).to be_a(Subscription) }
      it { expect(subject).to be_valid }
      it { expect { subject }.to change { Subscription.count }.by(1) }
    end

    context 'when device is allready subscribed' do
      let(:device) do
        {
          token: '123'
        }
      end
      before(:each) { FactoryGirl.create :subscription, token: '123', resource: diagram }
      subject { Subscribe.new(diagram).call(device) }

      it { expect(subject).to be_a(Subscription) }
      it { expect(subject).not_to be_valid }
      it { expect { subject }.not_to change { Subscription.count } }
    end
  end
end
