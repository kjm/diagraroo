require 'rails_helper'

describe AssignCanvasAttributes do
  let(:diagram) { FactoryGirl.create :diagram }

  describe '#call' do
    context 'when valid attributes' do
      let(:params) do
        {
          label: '123',
          x: 10,
          y: 10,
          canvas_type: Canvas::RECTANGLE,
          dimensions: { 'x' => 1, 'y' => 2 },
          diagram_id: diagram.id
        }
      end

      it { expect(AssignCanvasAttributes.new(Canvas.new, params).call).to be_a(Canvas) }
      it { expect(AssignCanvasAttributes.new(Canvas.new, params).call).to be_valid }
    end

    context 'when dimensions don\'t match specified shape' do
      let(:params) do
        {
          label: '123',
          x: 10,
          y: 10,
          canvas_type: Canvas::CIRCLE,
          dimensions: { 'x' => 1, 'y' => 2 },
          diagram_id: diagram.id
        }
      end
      subject do
        canvas = AssignCanvasAttributes.new(Canvas.new, params).call
        canvas.validate
        canvas
      end

      it { expect(subject).not_to be_valid }
      it { expect(subject.errors).to have_key(:dimensions) }
    end

    context 'when required fields are missing' do
      subject do
        canvas = AssignCanvasAttributes.new(Canvas.new, {}).call
        canvas.validate
        canvas
      end

      it { expect(subject).not_to be_valid }
      it { expect(subject.errors).to have_key(:label) }
      it { expect(subject.errors).to have_key(:canvas_type) }
      it { expect(subject.errors).to have_key(:dimensions) }
      it { expect(subject.errors).to have_key(:diagram) }
    end
  end
end
