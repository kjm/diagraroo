require 'rails_helper'

describe Unsubscribe do
  let(:diagram) { FactoryGirl.create :diagram }
  let!(:subscription) { FactoryGirl.create :subscription, token: '123', resource: diagram }

  describe '#call' do
    context 'when valid device' do
      let(:device) do
        {
          token: '123'
        }
      end
      subject { Unsubscribe.new(subscription.resource).call(device) }

      it { expect(subject).to be_a(Unsubscribe) }
      it { expect(subject).to be_success }
      it { expect { subject }.to change { Subscription.count }.by(-1) }
    end

    context 'when invalid device' do
      let(:device) do
        {
          token: '234234234'
        }
      end
      subject { Unsubscribe.new(subscription.resource).call(device) }

      it { expect(subject).to be_a(Unsubscribe) }
      it { expect(subject).not_to be_success }
      it { expect { subject }.not_to change { Subscription.count } }
    end
  end
end
