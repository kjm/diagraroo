require 'rails_helper'

describe BuildNotifications do
  let!(:diagram) { FactoryGirl.create :diagram }
  let!(:canvas) { FactoryGirl.create :circle_canvas, diagram: diagram }
  let!(:subscription) { FactoryGirl.create :subscription, resource: diagram }

  context 'when diagram is given as a resource' do
    [
      ['created', BuildNotifications::CREATED],
      ['updated', BuildNotifications::UPDATED],
      ['deleted', BuildNotifications::DELETED]
    ].each do |method, action_type|
      describe "##{method}" do
        it 'should compose proper message and iterate through own subscribers' do
          subscription_token = ''
          subscription_message = {}

          BuildNotifications.new(diagram).send(method) do |token, message|
            subscription_token = token
            subscription_message = message
          end

          expect(subscription_token).to eq(subscription.token)
          expect(subscription_message).to eq(action: action_type,
                                             resource: diagram.class.name,
                                             id: diagram.id)
        end
      end
    end
  end

  context 'when canvas is given as a resource' do
    [
      ['created', BuildNotifications::CREATED],
      ['updated', BuildNotifications::UPDATED],
      ['deleted', BuildNotifications::DELETED]
    ].each do |method, action_type|
      describe "##{method}" do
        it 'should compose proper message and iterate through diagram\'s subscribers' do
          subscription_token = ''
          subscription_message = {}

          BuildNotifications.new(canvas).send(method) do |token, message|
            subscription_token = token
            subscription_message = message
          end

          expect(subscription_token).to eq(subscription.token)
          expect(subscription_message).to eq(action: action_type,
                                             resource: canvas.class.name,
                                             id: canvas.id)
        end
      end
    end
  end
end
