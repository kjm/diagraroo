require 'rails_helper'

describe Shapes::IsNumber do
  subject { Object.new.extend Shapes::IsNumber }

  it { expect(subject.number?(5)).to be_truthy }
  it { expect(subject.number?('5')).to be_truthy }
  it { expect(subject.number?(0)).to be_truthy }
  it { expect(subject.number?('3.2')).to be_truthy }
  it { expect(subject.number?(3.22)).to be_truthy }
  it { expect(subject.number?(nil)).to be_falsy }
  it { expect(subject.number?(true)).to be_falsy }
  it { expect(subject.number?(false)).to be_falsy }
end
