require 'rails_helper'

describe Shapes::Circle do
  subject { Shapes::Circle.new(r: 3) }

  it { expect(subject).to respond_to(:to_h) }
  it { expect(subject).to be_valid }
end
