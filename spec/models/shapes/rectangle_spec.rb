require 'rails_helper'

describe Shapes::Rectangle do
  subject { Shapes::Rectangle.new(x: 3, y: 55) }

  it { expect(subject).to respond_to(:to_h) }
  it { expect(subject).to be_valid }
end
