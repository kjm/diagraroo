FactoryGirl.define do
  factory :diagram do
    name { |n| "Diagram #{n}" }
  end
end
