FactoryGirl.define do
  factory :canvas do
    label { |n| "Label #{n}" }
    x { rand(100) }
    y { rand(100) }
    canvas_type nil
    dimensions { {} }
    diagram

    trait(:circle) do
      canvas_type Canvas::CIRCLE
      dimensions { { r: rand(100) } }
    end

    trait(:rectangle) do
      canvas_type Canvas::RECTANGLE
      dimensions { { x: rand(100), y: rand(100) } }
    end

    factory :circle_canvas, traits: [:circle]
    factory :rectangle_canvas, traits: [:rectangle]
  end
end
