FactoryGirl.define do
  factory :subscription do
    token { |n| "123#{n}" }
  end
end
