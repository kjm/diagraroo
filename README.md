# Diagraroo

## How to run

- git clone git@bitbucket.org:kjm/diagraroo.git
- cd diagraroo
- use 2.1.2@diagraroo --create
- use 2.1.2@diagraroo
- bundle
- rake db:create
- rake db:schema:load
- rails s

## API Documentation

    http://localhost:3000/api/swagger

## Dependencies

- Ruby 2.1.2
- PostgreSQL 9.4.4

----

## Description

### Test project for Back-end Engineer (Ruby)

A group of developers wants to create a simple iPad application for collaborative, real time
diagram editing. They lack know how in the back end department, so they want you to help
them out.

#### Requirements

The API, written in Ruby, should allow users to add, delete and move shapes around the
canvas, with the changes being propagated to all users currently editing a diagram. The current
state of the diagram should be persisted in a database.
Each diagram should have a limitless canvas. The center of such canvas is at (0, 0). Ideally,
there should be a way of creating, deleting and listing them.
Support two types of shapes: rectangle and circle. It should be fairly easy to support new types
of shapes. Each shape instance should have a string label associated with it and users should
be able to change it.

Come up with a way of resolving editing conflicts, like move-after-delete.
Make it easy for other developers to start working on the project. Specifically, provide facilities to
test the back­end e.g. via the use of Docker and provisioning scripts.
Value quality over quantity. Follow unofficial Ruby style guide and use Rubocop to enforce it.
Your work should be available via GitHub or BitBucket as a private repository. Commit often.
Finally, when in doubt – especially about the scope of the task – ask!

#### Bonus

- Ensure scalability of the back end by providing support for multiple instances of isolated
back end application instances, for example via PubSub (Redis, RabbitMQ).
- Prepare RSpec tests for core components