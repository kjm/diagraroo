class Subscription < ActiveRecord::Base
  belongs_to :resource, polymorphic: true

  validates :token, presence: true
  validates :resource, presence: true
  validates_uniqueness_of :token, scope: [:resource_type, :resource_id]
end
