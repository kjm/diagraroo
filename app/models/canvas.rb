class Canvas < ActiveRecord::Base
  self.table_name = 'canvasses'

  CANVAS_TYPES = [CIRCLE = 1, RECTANGLE = 2]

  belongs_to :diagram

  attr_accessor :shape

  validates :label, presence: true
  validates :x, presence: true, numericality: true
  validates :y, presence: true, numericality: true
  validates :canvas_type, presence: true, numericality: true
  validates :dimensions, presence: true
  validates :diagram, presence: true
  validate :known_canvas_type
  validate :valid_shape_dimensions

  def subscriptions
    diagram.subscriptions
  end

  private

  def known_canvas_type
    errors.add(:canvas_type, 'unknown') unless CANVAS_TYPES.include? canvas_type
  end

  def valid_shape_dimensions
    unless @shape.nil?
      errors.add(:dimensions, @shape.error) unless @shape.valid?
    end
  end
end
