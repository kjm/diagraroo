class Diagram < ActiveRecord::Base
  has_many :canvasses, class_name: 'Canvas', dependent: :destroy
  has_many :subscriptions, as: :resource, dependent: :destroy
end
