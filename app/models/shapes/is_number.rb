module Shapes
  module IsNumber
    def number?(string)
      true if Float(string)
    rescue
      false
    end
  end
end
