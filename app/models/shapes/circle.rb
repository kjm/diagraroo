module Shapes
  class Circle < Base
    attr_accessor :r

    def initialize(hash)
      @r = hash[:r]
    end

    def valid?
      number? @r
    end

    def to_h
      { r: @r }
    end

    def error
      'did not contain :r dimension'
    end
  end
end
