module Shapes
  class Base
    include IsNumber

    def valid?
      fail 'This method is not implemented'
    end

    def to_h
      fail 'This method is not implemented'
    end

    def error
      'has invalid dimensions'
    end
  end
end
