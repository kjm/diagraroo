module Shapes
  class Rectangle < Base
    attr_accessor :x, :y

    def initialize(hash)
      @x = hash[:x]
      @y = hash[:y]
    end

    def valid?
      number?(@x) && number?(@y)
    end

    def to_h
      { x: @x, y: @y }
    end

    def error
      'did not contain :x, :y dimensions'
    end
  end
end
