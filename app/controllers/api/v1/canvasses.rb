module API
  module V1
    class Canvasses < Grape::API
      include API::V1::Defaults

      resource :canvasses do
        desc 'Returns single canvas'
        params do
          requires :id, type: Integer, desc: 'Canvas id'
        end
        get '/:id' do
          canvas = Canvas.find(declared(params)[:id])
          CanvasSerializer.new(canvas)
        end

        desc 'Creates new canvas'
        params do
          requires :canvas, type: Hash do
            requires :label, type: String, desc: 'Label'
            optional :x, type: Integer, desc: 'Center point, position on x-axis'
            optional :y, type: Integer, desc: 'Center point, position on y-axis'
            requires :canvas_type, type: Integer, desc: "Shape type: #{Canvas::CIRCLE} for circle or #{Canvas::RECTANGLE} for rectangle"
            requires :dimensions, type: Hash do
              optional :x, type: Integer, desc: 'Width of rectangle shape'
              optional :y, type: Integer, desc: 'Height of rectangle shape'
              optional :r, type: Integer, desc: 'Radius of circle shape'
            end
            requires :diagram_id, type: Integer, desc: 'Id of diagram, to which canvas belongs'
          end
        end
        post '/' do
          input = declared(params)[:canvas]
          canvas = Canvas.new

          canvas = AssignCanvasAttributes.new(canvas, input).call

          if canvas.save
            BuildNotifications.new(canvas).created do |token, message|
              APNService.push_notification(token, message)
            end
            CanvasSerializer.new(canvas)
          else
            error!(canvas.errors.full_messages, 422)
          end
        end

        desc 'Updates single canvas'
        params do
          requires :id, type: Integer, desc: 'Canvas id'
          requires :canvas, type: Hash do
            requires :label, type: String, desc: 'Label'
            optional :x, type: Integer, desc: 'Center point, position on x-axis'
            optional :y, type: Integer, desc: 'Center point, position on y-axis'
            requires :canvas_type, type: Integer, desc: "Shape type: #{Canvas::CIRCLE} for circle or #{Canvas::RECTANGLE} for rectangle"
            requires :dimensions, type: Hash do
              optional :x, type: Integer, desc: 'Width of rectangle shape'
              optional :y, type: Integer, desc: 'Height of rectangle shape'
              optional :r, type: Integer, desc: 'Radius of circle shape'
            end
            requires :diagram_id, type: Integer, desc: 'Id of diagram, to which canvas belongs'
          end
        end
        patch '/:id' do
          input = declared(params)[:canvas]
          canvas = Canvas.find(declared(params)[:id])

          canvas = AssignCanvasAttributes.new(canvas, input).call

          if canvas.save
            BuildNotifications.new(canvas).updated do |token, message|
              APNService.push_notification(token, message)
            end
            CanvasSerializer.new(canvas)
          else
            error!(canvas.errors.full_messages, 422)
          end
        end

        desc 'Deletes single canvas'
        params do
          requires :id, type: Integer, desc: 'Canvas id'
        end
        delete '/:id' do
          canvas = Canvas.find(params[:id])
          canvas.delete

          BuildNotifications.new(canvas).deleted do |token, message|
            APNService.push_notification(token, message)
          end
        end
      end
    end
  end
end
