module API
  module V1
    module Defaults
      extend ActiveSupport::Concern

      included do
        version 'v1'
        format :json

        rescue_from :all, backtrace: true
        rescue_from ActiveRecord::RecordNotFound do |_e|
          rack_response('{ "status": 404, "message": "Not Found." }', 404)
        end
      end
    end
  end
end
