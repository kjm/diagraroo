module API
  module V1
    class Diagrams < Grape::API
      include API::V1::Defaults

      resource :diagrams do
        desc 'Returns all diagrams'
        get '/' do
          ActiveModel::ArraySerializer.new(
            Diagram.all,
            each_serializer: DiagramOnListSerializer
          )
        end

        desc 'Returns single diagram'
        params do
          requires :id, type: Integer, desc: 'Diagram id'
        end
        get '/:id' do
          diagram = Diagram.find(params[:id])
          DiagramSerializer.new(diagram)
        end

        desc 'Creates new diagram'
        params do
          requires :diagram, type: Hash do
            requires :name, type: String, desc: 'Diagram name'
          end
        end
        post '/' do
          diagram = Diagram.new
          diagram.name = declared(params)[:diagram][:name]
          if diagram.save
            DiagramSerializer.new(diagram)
          else
            error!(diagram.errors.full_messages, 422)
          end
        end

        desc 'Updates single diagram'
        params do
          requires :id, type: Integer, desc: 'Diagram id'
          requires :diagram, type: Hash do
            requires :name, type: String, desc: 'Diagram name'
          end
        end
        patch '/:id' do
          diagram = Diagram.find(params[:id])
          diagram.name = declared(params)[:diagram][:name]
          if diagram.save
            BuildNotifications.new(diagram).updated do |token, message|
              APNService.push_notification(token, message)
            end
            DiagramSerializer.new(diagram)
          else
            error!(diagram.errors.full_messages, 422)
          end
        end

        desc 'Deletes single diagram'
        params do
          requires :id, type: Integer, desc: 'Diagram id'
        end
        delete '/:id' do
          diagram = Diagram.find(params[:id])
          notifications = BuildNotifications.new(diagram).deleted

          diagram.delete

          notifications.each do |token, message|
            APNService.push_notification(token, message)
          end
        end

        desc 'Subscribes a device to resource push messages'
        params do
          requires :id, type: Integer, desc: 'Diagram id'
          requires :device, type: Hash do
            requires :token, type: String, desc: 'Device token'
          end
        end
        patch '/:id/subscribe' do
          diagram = Diagram.find(params[:id])
          subscription = Subscribe.new(diagram).call(declared(params)[:device])

          unless subscription.valid?
            error!(subscription.errors.full_messages, 422)
          end
        end

        desc 'Unsubscribes a device from resource push messages'
        params do
          requires :id, type: Integer, desc: 'Diagram id'
          requires :device, type: Hash do
            requires :token, type: String, desc: 'Device token'
          end
        end
        patch '/:id/unsubscribe' do
          diagram = Diagram.find(params[:id])
          unsubscribe = Unsubscribe.new(diagram).call(declared(params)[:device])

          error!(unsubscribe.errors, 422) unless unsubscribe.success?
        end
      end
    end
  end
end
