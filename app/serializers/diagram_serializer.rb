class DiagramSerializer < ActiveModel::Serializer
  attributes :id, :name, :created_at, :updated_at, :canvasses

  def canvasses
    ActiveModel::ArraySerializer.new(object.canvasses, each_serializer: CanvasSerializer)
  end
end
