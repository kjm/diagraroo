class CanvasSerializer < ActiveModel::Serializer
  attributes :id, :label, :x, :y, :dimensions, :canvas_type, :created_at, :updated_at
end
