# TODO: implement using https://github.com/rpush/rpush or similar library

class APNService
  def self.push_notification(token, data)
    Rails.logger.info "Push message to #{token}: #{data.to_json}" unless Rails.env.test?
  end
end
