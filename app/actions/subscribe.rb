class Subscribe
  def initialize(resource)
    @resource = resource
  end

  def call(device)
    s = Subscription.new
    s.resource = @resource
    s.token = device[:token]
    s.save

    s
  end
end
