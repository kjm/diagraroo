class Unsubscribe
  attr_reader :errors

  def initialize(resource)
    @resource = resource
    @errors = []
  end

  def call(device)
    @errors = []

    subscription = @resource.subscriptions.where(token: token(device)).first

    if subscription
      subscription.delete
    else
      @errors << 'Subscription not found' if subscription.nil?
    end

    self
  end

  def success?
    @errors.count == 0
  end

  private

  def token(device)
    device.symbolize_keys.fetch(:token)
  end
end
