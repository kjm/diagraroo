class BuildNotifications
  PUSH_NOTIFICATION_TYPES = [CREATED = 'created', UPDATED = 'updated', DELETED = 'deleted']

  def initialize(object)
    @object = object
    @subscriptions = @object.subscriptions
  end

  def created(&block)
    @notification_type = CREATED
    each(&block)
  end

  def updated(&block)
    @notification_type = UPDATED
    each(&block)
  end

  def deleted(&block)
    @notification_type = DELETED
    each(&block)
  end

  private

  def each
    return enum_for(:each) unless block_given?

    @subscriptions.each do |subscription|
      yield subscription.token, message
    end
  end

  def message
    @message ||= create_message
  end

  def create_message
    {
      action: @notification_type,
      resource: @object.class.name,
      id: @object.id
    }
  end
end
