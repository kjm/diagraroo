class AssignCanvasAttributes
  def initialize(canvas, params)
    @canvas = canvas
    @params = params.deep_symbolize_keys
  end

  def call
    @canvas.label = @params[:label]
    @canvas.x = @params[:x].to_i || 0
    @canvas.y = @params[:y].to_i || 0
    @canvas.canvas_type = @params[:canvas_type].to_i
    @canvas.shape = build_shape
    @canvas.dimensions = @canvas.shape.to_h
    @canvas.diagram_id = @params[:diagram_id].to_i

    @canvas
  end

  private

  def build_shape
    case @params[:canvas_type].to_i
    when Canvas::RECTANGLE then Shapes::Rectangle.new(dimensions)
    when Canvas::CIRCLE then Shapes::Circle.new(dimensions)
    end
  end

  def dimensions
    @params[:dimensions] || {}
  end
end
